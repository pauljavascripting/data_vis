import React, {Component, PropTypes} from 'react';
import ProgressArc1 from './ProgressArc1'
import Tile1 from './Tile1'

export default class Results extends Component {
  render() {

    const key = ''
    const percentage = '0.85'
    
    return (
      
    <div className="summary">
        
       <Tile1
          key={key}
          tileStyle={{
          width: 180,
          height: 180,
          flexShrink: 0,
          borderRadius: '50%',
          alignItems: 'center',
          justifyContent: 'center'
          }}
          content={
            <ProgressArc1
              data={{
                value: percentage,
                  text: key
              }}
              arcID={`test--svg--${key}`}
              height={140}
              width={140}
              pathStyle={{
                width: 20,
                  fill: 'green'

              }}
              valueClass={'summary--tiles--value'}
              labelClass={'summary--tiles--label'}
              />
          }
          />

    </div>
  )}
};
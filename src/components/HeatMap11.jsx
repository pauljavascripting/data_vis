import React, { Component } from 'react';
import { Map, CircleMarker, ImageOverlay } from "react-leaflet";
import {CRS} from 'leaflet';
import "leaflet/dist/leaflet.css";
import mapImg from '../images/0003_boulder.png'
import HeatmapLayer from 'react-leaflet-heatmap-layer'
import '../App.css'

import axios from 'axios'

const position = [0,0]
const imageBounds = [[-100,-100], [100,100]];

const JSON_URL = process.env.PUBLIC_URL+'/Map8km_Main.json'

//-------------------------------------------------------------------
//
// https://towardsdatascience.com/creating-a-bubbles-map-using-react-leaflet-e75124ca1cd2
// https://github.com/OpenGov/react-leaflet-heatmap-layer/blob/master/example/realworld.10000.js
// https://stackoverflow.com/questions/55659617/react-leaflet-rotating-imageoverlay-react-component-by-n-degrees
//
//-------------------------------------------------------------------

export default class HeatMap11 extends Component {

  state = { zoom:1, minZoom:1, maxZoom:10, dragging:true, array:[] }

  // static propTypes = {
  //   integrationName: PropTypes.string.isRequired,
  //   connected: PropTypes.bool.isRequired,
  //   handleConnect: PropTypes.func.isRequired,
  //   handleDisconnect: PropTypes.func.isRequired,
  //   disabled: PropTypes.bool
  // };

  // heatmap -zoom out & still see same points?
  // try freezer
  // try markers for each point

  componentDidMount(){

    axios.get(JSON_URL)
      
      .then((response) => {

        const data = response.data

        const array = []

        for(let i=0;i<data.length;i++){

          const tempArray = []
           tempArray.push((data[i].x)/-4500)
          tempArray.push((data[i].y)/-4500)

          array.push(tempArray)

        }
        console.log(array.length)
        this.setState({array:array})

      })

  }

  render() {

    return (
        <div style={{ height: '100vh', width: '100%' }}>
        <Map
          style={{ height: "100%", width: "100%" }}
          zoom={this.state.zoom}
          center={position}
          maxBoundsViscosity={1}
          ref={(ref) => { this.map = ref; }}
          onMove={this.onUpdate}
          minZoom={this.state.minZoom}
          maxZoom={this.state.maxZoom}
          dragging={this.state.dragging}
          crs={CRS.Simple}
          >
          
           <ImageOverlay
                bounds={imageBounds}
                url={mapImg}
                transparent={true}
                opacity={0.19}
                className='overlay'
            />

             <HeatmapLayer
                   fitBoundsOnLoad
                    points={this.state.array}
                    longitudeExtractor={m => m[1]}
                    latitudeExtractor={m => m[0]}
                    intensityExtractor={m => parseFloat(m[2])}
                    radius={1}
                    gradient={{0.4: 'white', 0.8: 'orange', 1.0: 'red'}}
                    max={1}
                    blur={1}

                  />

          <CircleMarker
            center={position}
          />
        
        </Map>
      </div>
    );
  }
}


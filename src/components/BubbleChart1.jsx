import React, {Component, PropTypes} from 'react';
import BubbleChart from './BubbleChart';

const data1 = [
{ label: 'Torpdeo', value:1, fish:'carp' },
{ label: 'Woody', value: 1, fish:'carp' },
{ label: 'The Social Common', value: 4, fish:'carp' },
{ label: 'Max', value: 22, fish:'carp' },]

export default class BubbleChart1 extends Component {

  // static propTypes = {
  //   integrationName: PropTypes.string.isRequired,
  //   connected: PropTypes.bool.isRequired,
  //   handleConnect: PropTypes.func.isRequired,
  //   handleDisconnect: PropTypes.func.isRequired,
  //   disabled: PropTypes.bool
  // };

  componentDidMount(){

  }

  bubbleClick = (label) =>{
  console.log("Custom bubble click func")
  }

  render() {
    return (
      <div>
      Trophy Fish Discovered
      <div>
        <BubbleChart
          graph= {{
            zoom: 1.1,
            offsetX: -0.05,
            offsetY: -0.01,
          }}
          width={1000}
          height={800}
          padding={0} // optional value, number that set the padding between bubbles
          showLegend={true} // optional value, pass false to disable the legend.
          legendPercentage={20} // number that represent the % of with that legend going to use.
          legendFont={{
                family: 'Arial',
                size: 12,
                color: '#000',
                weight: 'bold',
              }}
          valueFont={{
                family: 'Arial',
                size: 12,
                color: '#fff',
                weight: 'bold',
              }}
          labelFont={{
                family: 'Arial',
                size: 20,
                color: '#fff',
                weight: 'bold',
              }}
          //Custom bubble/legend click functions such as searching using the label, redirecting to other page
          bubbleClickFunc={this.bubbleClick}
          legendClickFun={this.legendClick}
          data={data1}
        />
      </div>
      </div>
    );
  }
}


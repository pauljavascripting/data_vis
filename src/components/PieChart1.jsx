import React, {Component, PropTypes} from 'react';

import {
  PieChart, Pie, Sector, Cell, Legend,
} from 'recharts';

import data2 from '../data/data.json'

// outerRadius defines size of chart


const data = [
  { name: 'Arapaima', value: 100 },
  { name: 'Spotted Bass', value: 150 },
  { name: 'Smallmouth Bass', value: 250},
  { name: 'Black Pacu', value: 500 },
];

const data1 = [
  { name: 'Bass1', value: 300 },
  { name: 'Bass2', value: 300 },
  { name: 'Bass3', value: 300 },
  { name: 'Bass4', value: 100 },
];

const COLORS = ['#6c6673', '#967550', '#997d63', '#65594f'];

const RADIAN = Math.PI / 180;

export default class PieChart1 extends Component {

  state = {data:data}

  componentDidMount(){

    console.log(data2.fishDiscovered)
  }

  // static propTypes = {
  //   integrationName: PropTypes.string.isRequired,
  //   connected: PropTypes.bool.isRequired,
  //   handleConnect: PropTypes.func.isRequired,
  //   handleDisconnect: PropTypes.func.isRequired,
  //   disabled: PropTypes.bool
  // };

  clicked = (label) => {

    this.setState({data:data1})
  }

  reset = () => {

    this.setState({data:data})

  }

  renderCustomizedLabel = (data) => {
    
    const name = data.name;

    const radius = data.innerRadius + (data.outerRadius - data.innerRadius) * 0.5;
    const x = data.cx + radius * Math.cos(-data.midAngle * RADIAN);
    const y = data.cy + radius * Math.sin(-data.midAngle * RADIAN);

    return (
      <text x={x} y={y} fontSize="10" fill="white" textAnchor={x > data.cx ? 'start' : 'middle'} dominantBaseline="central">
     {name}({data.percent*100}%)
     </text>
    );
  };

  render() {

   return (
      <div>
      Fish Species Discovered
      <button onClick={this.reset}>Reset</button>
     <DrawPie data={this.state.data} click={this.clicked} label={this.renderCustomizedLabel} />
      
      </div>
    );
  }
}

const DrawPie = (props) => {

return (
  <PieChart width={400} height={400}>
        <Pie
          data={props.data}
          cx={200}
          cy={200}
          labelLine={false}
          label={props.label}
          outerRadius={180}
          fill="#8884d8"
          dataKey="value"
          onClick={props.click}
          
        >
        <Legend verticalAlign="top" height={36}/>
          {
            props.data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
          }
        </Pie>
      </PieChart>
)

}
import React, {Component, PropTypes} from 'react';

import {
  PieChart, Pie, Sector, Cell, Legend,
} from 'recharts';

import data2 from '../data/data.json'

const _ = require('lodash');

// outerRadius defines size of chart


// const data0 = [
//   { fish_species: "Carp", count: 1 },
//   { fish_species: "Bass", count: 2 },
//   { fish_species: "Predator", count: 4 },
//   { fish_species: "Other", count: 9 },
// ];

const data0 = [
  
  {fish_species: "Bass", count: 20},
  {fish_species: "Carp", count: 18},
  {fish_species: "Catfish", count: 2},
  {fish_species: "Crappie", count: 1},
  {fish_species: "None", count: 36},
  {fish_species: "Perch", count: 4},
  {fish_species: "Trout", count: 1}

];

const data1 = [
  { fish_species: "Carp", fish:[ {fish_species:'Common Carp', count:1}  ] },
  { fish_species: "Bass", fish:[ {fish_species:'Bass1', count:1}, {fish_species:'Bass2', count:1}  ] },
  { fish_species: "Predator", fish:[ {fish_species:'Predator1', count:1}, {fish_species:'Predator2', count:2}, {fish_species:'Predator3', count:1},   ] },
  { fish_species: "Trout", fish:[ {fish_species:'Other1', count:1}, {fish_species:'Other2', count:1}, {fish_species:'Other3', count:1}, {fish_species:'Other4', count:6}  ] },
];


const COLORS = ['#6c6673', '#967550', '#997d63', '#65594f'];

const RADIAN = Math.PI / 180;

export default class PieChart3 extends Component {

  //state = {data:data2.fishDiscovered}
  state = {data:data0}

  componentDidMount(){

    console.log(data0)
  }

  // static propTypes = {
  //   integrationName: PropTypes.string.isRequired,
  //   connected: PropTypes.bool.isRequired,
  //   handleConnect: PropTypes.func.isRequired,
  //   handleDisconnect: PropTypes.func.isRequired,
  //   disabled: PropTypes.bool
  // };

  clicked = (label) => {
    console.log(label)
    const info = _.filter(data1, function(p) {
      return p.fish_species === label.fish_species;
    });

    if(info[0]){
      this.setState({data:info[0].fish})  
    }
    
  }

  reset = () => {

    this.setState({data:data0})

  }

  renderCustomizedLabel = (data) => {
    
    const name = data.fish_species;//name;

    const radius = data.innerRadius + (data.outerRadius - data.innerRadius) * 0.5;
    const x = data.cx + radius * Math.cos(-data.midAngle * RADIAN);
    const y = data.cy + radius * Math.sin(-data.midAngle * RADIAN);
    const roundedPercent = Math.round(data.percent*100);
    return (
      <text x={x} y={y} fontSize="10" fill="white" textAnchor={x > data.cx ? 'start' : 'middle'} dominantBaseline="central">
     {name}({roundedPercent}%)
     </text>
    );
  };

  render() {

   return (
      <div>
      Fish Species Discovered
      <button onClick={this.reset}>Reset</button>
     <DrawPie data={this.state.data} click={this.clicked} label={this.renderCustomizedLabel} />
      
      </div>
    );
  }
}

const DrawPie = (props) => {

return (
  <PieChart width={400} height={400}>
        <Pie
          data={props.data}
          cx={200}
          cy={200}
          labelLine={false}
          label={props.label}
          outerRadius={180}
          fill="#8884d8"
          dataKey="count"
          onClick={props.click}
          
        >
        <Legend verticalAlign="top" height={36}/>
          {
            props.data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
          }
        </Pie>
      </PieChart>
)

}
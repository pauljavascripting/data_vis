import React, {Component, PropTypes} from 'react';

import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

import data2 from '../data/data.json'

const data = [
  {
    time_of_day: "Sunrise", time_spent_in_event: 25571.508927, time_spent_fishing: 17544.526734, total_fish: 30, total_length: 26.597503, total_weight: 368.684327
  },
  {
    time_of_day: "Sunset", time_spent_in_event: 25571.508927, time_spent_fishing: 1544.526734, total_fish: 41, total_length: 26.597503, total_weight: 368.684327
  },
  {
    time_of_day: "Morning", time_spent_in_event: 25571.508927, time_spent_fishing: 174.526734, total_fish: 10, total_length: 26.597503, total_weight: 368.684327
  },
  {
    time_of_day: "Afternoon", time_spent_in_event: 25571.508927, time_spent_fishing: 17794.526734, total_fish: 20, total_length: 26.597503, total_weight: 368.684327
  },
  {
    time_of_day: "Evening", time_spent_in_event: 25571.508927, time_spent_fishing: 204.526734, total_fish: 10, total_length: 26.597503, total_weight: 368.684327
  },
  {
    time_of_day: "Midnight", time_spent_in_event: 25571.508927, time_spent_fishing: 4.526734, total_fish: 2, total_length: 26.597503, total_weight: 368.684327
  },
  
];

export default class LineChart2 extends Component {

  // static propTypes = {
  //   integrationName: PropTypes.string.isRequired,
  //   connected: PropTypes.bool.isRequired,
  //   handleConnect: PropTypes.func.isRequired,
  //   handleDisconnect: PropTypes.func.isRequired,
  //   disabled: PropTypes.bool
  // };

  componentDidMount(){

    console.log(data2.favouriteTime)
  }


  render() {
    return (
      <div>
      <div>
      Favourite Time of Day for Fishing
        <LineChart
        width={500}
        height={300}
        data={data}
        margin={{
          top: 5, right: 30, left: 20, bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="time_of_day" />
        <YAxis />
        <Tooltip />
        <Line type="monotone" dataKey="time_spent_fishing" stroke="#8884d8" activeDot={{ r: 8 }} />

      </LineChart>
      </div>
      <div>
      Best Time for catching fish
        <LineChart
        width={500}
        height={300}
        data={data}
        margin={{
          top: 5, right: 30, left: 20, bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="time_of_day" />
        <YAxis />
        <Tooltip />
        <Line type="monotone" dataKey="total_fish" stroke="#8884d8" activeDot={{ r: 8 }} />

      </LineChart>
      </div>
      </div>
    );
  }
}


import React, { Component } from 'react';
import { Map, CircleMarker, TileLayer, WMSTileLayer, ImageOverlay } from "react-leaflet";
import {CRS,point} from 'leaflet';
import "leaflet/dist/leaflet.css";
import mapImg from '../images/0005_gigantica.png'
// import mapImg from '../images/0003_boulder.png'
import HeatmapLayer from 'react-leaflet-heatmap-layer'
import '../App.css'

import axios from 'axios'

const position = [0,0]
// const imageBounds = [[-125,-25], [25,125]]; // SW & NE
// const imageBounds = [[-200,-100], [0,100]];
const mapBounds = [[-100,-100], [100,100]];

// const mapBounds = [[-100, -167], [104, 167]]

const JSON_URL = process.env.PUBLIC_URL+'/gigantica.json'

//-------------------------------------------------------------------
//
// https://towardsdatascience.com/creating-a-bubbles-map-using-react-leaflet-e75124ca1cd2
// https://github.com/OpenGov/react-leaflet-heatmap-layer/blob/master/example/realworld.10000.js
// https://stackoverflow.com/questions/55659617/react-leaflet-rotating-imageoverlay-react-component-by-n-degrees
//
//-------------------------------------------------------------------

export default class HeatMap6 extends Component {

  state = { zoom:0.1, minZoom:1, maxZoom:10, dragging:true, array:[] }

  // static propTypes = {
  //   integrationName: PropTypes.string.isRequired,
  //   connected: PropTypes.bool.isRequired,
  //   handleConnect: PropTypes.func.isRequired,
  //   handleDisconnect: PropTypes.func.isRequired,
  //   disabled: PropTypes.bool
  // };

  componentDidMount(){

    // this.map.props.crs.transformation._a = 1 / 32
    // this.map.props.crs.transformation._b = 134
    // this.map.props.crs.transformation._c = -1
    // this.map.props.crs.transformation._d = 134

    // console.log(this.map.props.crs.transformation)

    // this.map.props.crs.transformation(1 / 32, 134, -1 / 32, 134)

    // load data -------------------------------------
    axios.get(JSON_URL)
      
      .then((response) => {

        const data = response.data

        const array = []

        for(let i=0;i<data.length;i++){

          if(data[i].x){
          const tempArray = []
          tempArray.push(data[i].x/-250)
          tempArray.push(data[i].y/-250)
          array.push(tempArray)

//           var pointXY = point(data[i].x, data[i].y, true);
//           var pointlatlng = this.map.props.crs.pointToLatLng(pointXY);
// //          console.log(pointXY)
//   //        console.log(pointlatlng)

          // array.push(pointlatlng)
          }

        }
        console.log(array.length)
        this.setState({array:array})

      })

  }

  mapReady = () => {

    // rotate heatmap
    const heatmap = this.refs.heatmap._el
    const ctx = heatmap.getContext("2d")
    //ctx.rotate(0.3)
    //ctx.translate(-450, -450);
    
    //console.log(this.refs.map)
  }

  render() {

    return (
        <div style={{ height: '100vh', width: '100%', border:'1px black solid'}}>
        <Map
          style={{ height: "100%", width: "100%" }}
          zoom={this.state.zoom}
          center={position}
          maxBoundsViscosity={1}
          ref={(ref) => { this.map = ref; }}
          onMove={this.onUpdate}
          minZoom={this.state.minZoom}
          maxZoom={this.state.maxZoom}
          dragging={this.state.dragging}
          crs={CRS.Simple}
          whenReady={this.mapReady}
          className="map"
          bounds={mapBounds}
          >
          
           <ImageOverlay
                bounds={mapBounds}
                url={mapImg}
                transparent={true}
                opacity={0.99}
                className='overlay'
            />

             <HeatmapLayer
                    fitBoundsOnLoad
                    ref="heatmap"
                    points={this.state.array}
                    longitudeExtractor={m => m[1]}
                    latitudeExtractor={m => m[0]}
                    intensityExtractor={m => parseFloat(m[2])}
                    radius={1}
                    gradient={{0.4: 'white', 0.8: 'orange', 1.0: 'red'}}
                    max={1}
                    blur={1}
                    className="heatmap"
                  />

          <CircleMarker
            center={position}
          />
        
        </Map>
      </div>
    );
  }
}


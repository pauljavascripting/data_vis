import React, { Component } from 'react';
import { Map, CircleMarker, TileLayer, WMSTileLayer, ImageOverlay } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import mapImg from '../images/0006_canal-edit-small.png'
import HeatmapLayer from 'react-leaflet-heatmap-layer'
import '../App.css'


const position = [51.4387, 0.30]

const imageBounds =[[51.3931, 0.2437],[51.5141, 0.4285]] // SW & NE

const addressPoints = [
[51.4387, 0.3120, "0.6"],

[51.4411, 0.3369, "571"],

[51.4160, 0.3201, "1070"]
]

// const gradientArray = {[[0: "#000000"],[0.2: "#570000"],[0.4: "#ff0000"],[0.6: "#ffc800"],[0.8: "#ffff00"],[1: "#FFFFFF"]}

/*
 fitBoundsOnLoad
  fitBoundsOnUpdate

*/


//-------------------------------------------------------------------
//
// https://towardsdatascience.com/creating-a-bubbles-map-using-react-leaflet-e75124ca1cd2
// https://github.com/OpenGov/react-leaflet-heatmap-layer/blob/master/example/realworld.10000.js
// https://stackoverflow.com/questions/55659617/react-leaflet-rotating-imageoverlay-react-component-by-n-degrees
//
//-------------------------------------------------------------------

export default class HeatMap2 extends Component {

  state = { zoom:14, minZoom:13, maxZoom:16, dragging:true }

  // static propTypes = {
  //   integrationName: PropTypes.string.isRequired,
  //   connected: PropTypes.bool.isRequired,
  //   handleConnect: PropTypes.func.isRequired,
  //   handleDisconnect: PropTypes.func.isRequired,
  //   disabled: PropTypes.bool
  // };


  onUpdate = (map) => {

   // if(this.map.leafletElement.getZoom()<=this.state.minZoom){

   //    this.setState({dragging:false})
   // }
   // else{

   //     this.setState({dragging:true})
   // }

  }

  render() {

    return (
        <div style={{ height: '100vh', width: '100%' }}>
        <Map
          style={{ height: "100%", width: "100%" }}
          zoom={this.state.zoom}
          center={position}
          maxBoundsViscosity={1}
          ref={(ref) => { this.map = ref; }}
          onMove={this.onUpdate}
          minZoom={this.state.minZoom}
          maxZoom={this.state.maxZoom}
          dragging={this.state.dragging}
          >
          <TileLayer url="http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
          
           <ImageOverlay
                bounds={imageBounds}
                url={mapImg}
                transparent={true}
                opacity={0.99}
                className='overlay'
            />

             <HeatmapLayer
                   
                    points={addressPoints}
                    longitudeExtractor={m => m[1]}
                    latitudeExtractor={m => m[0]}
                    intensityExtractor={m => parseFloat(m[2])}
                    radius={30}
                    gradient={{0.45: "rgb(0,0,255)"}}
                  />

          <CircleMarker
            center={position}
          />
        
        </Map>
      </div>
    );
  }
}

const styles = {

  overlay:{

    border:'1px red solid'
  }



}



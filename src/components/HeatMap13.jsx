import React, {Component} from 'react';
import PropTypes from 'prop-types';
import HeatMap from './HeatMap'
// import mapImg from '../images/0006_canal.png'
// import mapImg from '../images/0004_johnson.png'
// import mapImg from '../images/0002_manorfarm.png'
// import mapImg from '../images/0005_gigantica.png'
// import mapImg from '../images/williams.png'
// import mapImg from '../images/stawbestii_non_faded.png'
// import mapImg from '../images/0000_waldsee.png'
// import mapImg from '../images/0003_boulder.png'
import mapImg from '../images/0001_bergsee.png'

const data = {
      json: process.env.PUBLIC_URL+'/WisconsinMasterLevel.json',
      blur:1,
      bounds: [[-100,-100], [100,100]],
      centre: [0,0],
      dragging:true,
      gradient:{0.4: 'white', 0.8: 'orange', 1.0: 'red'},
      image: mapImg,
      minZoom:1,
      maxZoom:10,
      posXDivider:-4500,
      posYDivider:-4500,
      posX:0,
      posY:0,
      radius:1,
      zoom:1,
    };

// const data = {
//       json: process.env.PUBLIC_URL+'/Map8km_Main.json',
//       blur:1,
//       bounds: [[-100,-100], [100,100]],
//       centre: [0,0],
//       dragging:true,
//       gradient:{0.4: 'white', 0.8: 'orange', 1.0: 'red'},
//       image: mapImg,
//       minZoom:1,
//       maxZoom:10,
//       posXDivider:-4200,
//       posYDivider:-4200,
//       posX:0,
//       posY:0,
//       radius:1,
//       zoom:1,
//     };

// const data = {
//       json: process.env.PUBLIC_URL+'/WisconsinMasterLevel.json',
//       blur:1,
//       bounds: [[-100,-100], [100,100]],
//       centre: [0,0],
//       dragging:true,
//       gradient:{0.4: 'white', 0.8: 'orange', 1.0: 'red'},
//       image: mapImg,
//       minZoom:1,
//       maxZoom:10,
//       posXDivider:-4500,
//       posYDivider:-7500,
//       posX:0,
//       posY:0,
//       radius:1,
//       zoom:1,
//     };

// const data = {
//       json: process.env.PUBLIC_URL+'/StawBestii.json',
//       blur:1,
//       bounds: [[-100,-100], [100,100]],
//       centre: [0,0],
//       dragging:true,
//       gradient:{0.4: 'white', 0.8: 'orange', 1.0: 'red'},
//       image: mapImg,
//       minZoom:1,
//       maxZoom:10,
//       posXDivider:-300,
//       posYDivider:-300,
//       posX:-65000,
//       posY:-122000,
//       radius:1,
//       zoom:1,
//     };

// const data = {
//       json: process.env.PUBLIC_URL+'/Alabama.json',
//       blur:1,
//       bounds: [[-100,-100], [100,100]],
//       centre: [0,0],
//       dragging:true,
//       gradient:{0.4: 'white', 0.8: 'orange', 1.0: 'red'},
//       image: mapImg,
//       minZoom:1,
//       maxZoom:10,
//       posXDivider:-7000,
//       posYDivider:-7000,
//       posX:-50000,
//       posY:0,
//       radius:1,
//       zoom:1,
//     };


// const data = {
//       json: process.env.PUBLIC_URL+'/LakeMasterLevel.json',
//       blur:1,
//       bounds: [[-100,-100], [100,100]],
//       centre: [0,0],
//       dragging:true,
//       gradient:{0.4: 'white', 0.8: 'orange', 1.0: 'red'},
//       image: mapImg,
//       minZoom:1,
//       maxZoom:10,
//       posXDivider:-4500,
//       posYDivider:-4500,
//       posX:0,
//       posY:-150000,
//       radius:1,
//       zoom:1,
//     };

// const data = {
//       json: process.env.PUBLIC_URL+'/gigantica.json',
//       blur:1,
//       bounds: [[-100,-100], [100,100]],
//       centre: [0,0],
//       dragging:true,
//       gradient:{0.4: 'white', 0.8: 'orange', 1.0: 'red'},
//       image: mapImg,
//       minZoom:1,
//       maxZoom:10,
//       posXDivider:-250,
//       posYDivider:-250,
//       posX:0,
//       posY:0,
//       radius:1,
//       zoom:1,
//     };

// const data = {
//       json: process.env.PUBLIC_URL+'/manor3.json',
//       blur:1,
//       bounds: [[-100,-100], [100,100]],
//       centre: [0,0],
//       dragging:true,
//       gradient:{0.4: 'white', 0.8: 'orange', 1.0: 'red'},
//       image: mapImg,
//       minZoom:1,
//       maxZoom:10,
//       posXDivider:-200,
//       posYDivider:-200,
//       posX:-1000,
//       posY:+10000,
//       radius:1,
//       zoom:1,
//     };

// const data = {
//       json: process.env.PUBLIC_URL+'/johnson.json',
//       blur:1,
//       bounds: [[-100,-100], [100,100]],
//       centre: [0,0],
//       dragging:true,
//       gradient:{0.4: 'white', 0.8: 'orange', 1.0: 'red'},
//       image: mapImg,
//       minZoom:1,
//       maxZoom:10,
//       posXDivider:-4000,
//       posYDivider:-4000,
//       posX:0,
//       posY:0,
//       radius:1,
//       zoom:1,
//     };

// const data = {
//       json: process.env.PUBLIC_URL+'/canal.json',
//       blur:1,
//       bounds: [[-100,-100], [100,100]],
//       centre: [0,0],
//       dragging:true,
//       gradient:{0.4: 'white', 0.8: 'orange', 1.0: 'red'},
//       image: mapImg,
//       minZoom:1,
//       maxZoom:10,
//       posXDivider:-300,
//       posYDivider:-300,
//       posX:0,
//       posY:0,
//       radius:1,
//       zoom:1,
//     };

export default class HeatMap13 extends Component {

  render() {
    return (
      <div>
      Favourite Weather
      <HeatMap {...data} />
      </div>
    );
  }
}

HeatMap13.propTypes = {
  image: PropTypes.string
};
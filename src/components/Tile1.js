import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import * as d3 from 'd3';

export default class Tile1 extends React.Component {
    // getStyles = () => {
    //     return {
    //         tile: {
    //             width: 500,
    //             height: 250,
    //             display: 'flex',
    //             flexDirection: 'column',
    //             backgroundColor: '#ffffff',
    //             boxShadow: '0 0 5px rgba(25, 25, 25, 0.25)'
    //         },
    //         title: {
    //             borderBottom: '1px solid #D5DCE4',
    //             width: '100%',
    //             height: 30,
    //             display: 'flex',
    //             justifyContent: 'flex-start',
    //             alignItems: 'center'
    //         },
    //         text: {
    //             fontSize: '1em',
    //             marginLeft: 20
    //         }
    //     }
    // };

    render() {
       // const styles = this.getStyles();
       /*
       <div className={this.props.outerClass} style={Object.assign({}, styles.tile, this.props.tileStyle)}>
                {this.props.content}
            </div>
       */
        return (
            
            <div style={Object.assign({}, styles.tile, this.props.tileStyle)}>
                {this.props.content}
            </div>
        )
    }
}

const styles = {

    tile:{

        width: 500,
        height: 250,
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: '#ffffff',
        boxShadow: '0 0 5px rgba(25, 25, 25, 0.25)'

    }
}
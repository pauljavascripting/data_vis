import React, {Component, PropTypes} from 'react';
import GoogleMapReact from 'google-map-react';

const positions = [
  { lat: 51.4462, lng: 0.2169 },
  { lat: 51.2704, lng: 0.5227 },
];


const lat = 51.4462
const lng = 0.2169

const lat1 = 52.4462
const lng1 = 0.2169

let heatmap

export default class HeatMap1 extends Component {

  state = {heatmap:true, positions:positions, radius:60, opacity:0.5, lat:lat, lng:lng, zoom:11 }

  // static propTypes = {
  //   integrationName: PropTypes.string.isRequired,
  //   connected: PropTypes.bool.isRequired,
  //   handleConnect: PropTypes.func.isRequired,
  //   handleDisconnect: PropTypes.func.isRequired,
  //   disabled: PropTypes.bool
  // };

  //https://medium.com/@zjor/heatmaps-with-google-map-react-57e279315060
  //https://github.com/google-map-react/google-map-react-examples/blob/master/src/examples/Main.js#L69


  toggleHeatMap = () => {

    if(this.state.heatmap){

      this.setState({heatmap:false, positions:[]})
    }
    else{

      this.setState({heatmap:true, positions:positions})
    }
  }

  toggleRadius = () => {

    if(this.state.radius===60){

      this.setState({radius:90})

    }
    else{

      this.setState({radius:20})

    }

  }

  toggleOpacity = () => {

    if(this.state.opacity===0.5){

      this.setState({opacity:0.2})

    }
    else{

      this.setState({opacity:0.5})

    }

  }

  componentDidUpdate() {

    let map = this.state.map
    let maps = this.state.maps

    // add heat map on first load ---------------------------
    if(!heatmap){

      const heatMapData = [
      {location: new maps.LatLng(lat, lng) },
      {location: new maps.LatLng(lat1, lng1)},
      ];

      heatmap = new maps.visualization.HeatmapLayer({
        data: heatMapData
      });

      heatmap.setOptions({radius: this.state.radius, opacity:this.state.opacity});

      heatmap.setMap(map);

    }
    else{

        // update heat map ---------------------------
        heatmap.setOptions({radius: this.state.radius, opacity:this.state.opacity});

        heatmap.setMap(map);

    }
      
  }

  render() {

    return (
      <div>
      Heat Map
      <button onClick={this.toggleHeatMap}>Toggle Heatmap</button>
      <button onClick={this.toggleRadius}>Change Radius</button>
      <button onClick={this.toggleOpacity}>Change Opacity</button>
      
      <div style={{ height: '100vh', width: '100%' }}>
        
        <GoogleMapReact
         
          bootstrapURLKeys={{ key: 'AIzaSyBZRwJZQlqZ54k0WZJR7cwqUuQ3WnsUhSc' }}
          defaultCenter={[this.state.lat, this.state.lng]}
          defaultZoom={this.state.zoom}
          heatmapLibrary={this.state.heatmap}          
          ref='map'
          yesIWantToUseGoogleMapApiInternals
          onGoogleApiLoaded={({map, maps}) => this.setState({maps: maps, map: map})}
          
        >
          
        </GoogleMapReact>
      </div>
      </div>
    );
  }
}

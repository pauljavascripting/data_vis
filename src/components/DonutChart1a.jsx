import React, {Component, PropTypes} from 'react';
import * as d3 from 'd3';
// import {Donut3D} from './Donut3D'

const salesData=[
  {label:"Basic", color:"#3366CC"},
  {label:"Plus", color:"#DC3912"},
  {label:"Lite", color:"#FF9900"},
  {label:"Elite", color:"#109618"},
  {label:"Delux", color:"#990099"}
];

//http://bl.ocks.org/NPashaP/9994181
//http://bl.ocks.org/NPashaP/raw/9994181/Donut3D.js


export default class DonutChart1 extends Component {

  // static propTypes = {
  //   integrationName: PropTypes.string.isRequired,
  //   connected: PropTypes.bool.isRequired,
  //   handleConnect: PropTypes.func.isRequired,
  //   handleDisconnect: PropTypes.func.isRequired,
  //   disabled: PropTypes.bool
  // };

  randomData = () => {
  
    return salesData.map(function(d){ 

      return {label:d.label, value:1000*Math.random(), color:d.color};

    });
  
  }

  pieTop = (d, rx, ry, ir ) => {
    if(d.endAngle - d.startAngle == 0 ) return "M 0 0";
    var sx = rx*Math.cos(d.startAngle),
      sy = ry*Math.sin(d.startAngle),
      ex = rx*Math.cos(d.endAngle),
      ey = ry*Math.sin(d.endAngle);
      
    var ret =[];
    ret.push("M",sx,sy,"A",rx,ry,"0",(d.endAngle-d.startAngle > Math.PI? 1: 0),"1",ex,ey,"L",ir*ex,ir*ey);
    ret.push("A",ir*rx,ir*ry,"0",(d.endAngle-d.startAngle > Math.PI? 1: 0), "0",ir*sx,ir*sy,"z");
    return ret.join(" ");
  }

  pieOuter = (d, rx, ry, h ) => {
    var startAngle = (d.startAngle > Math.PI ? Math.PI : d.startAngle);
    var endAngle = (d.endAngle > Math.PI ? Math.PI : d.endAngle);
    
    var sx = rx*Math.cos(startAngle),
      sy = ry*Math.sin(startAngle),
      ex = rx*Math.cos(endAngle),
      ey = ry*Math.sin(endAngle);
      
      var ret =[];
      ret.push("M",sx,h+sy,"A",rx,ry,"0 0 1",ex,h+ey,"L",ex,ey,"A",rx,ry,"0 0 0",sx,sy,"z");
      return ret.join(" ");
  }

  pieInner = (d, rx, ry, h, ir ) =>{
    var startAngle = (d.startAngle < Math.PI ? Math.PI : d.startAngle);
    var endAngle = (d.endAngle < Math.PI ? Math.PI : d.endAngle);
    
    var sx = ir*rx*Math.cos(startAngle),
      sy = ir*ry*Math.sin(startAngle),
      ex = ir*rx*Math.cos(endAngle),
      ey = ir*ry*Math.sin(endAngle);

      var ret =[];
      ret.push("M",sx, sy,"A",ir*rx,ir*ry,"0 0 1",ex,ey, "L",ex,h+ey,"A",ir*rx, ir*ry,"0 0 0",sx,h+sy,"z");
      return ret.join(" ");
  }

  getPercent = (d) => {
    return (d.endAngle-d.startAngle > 0.2 ? 
        Math.round(1000*(d.endAngle-d.startAngle)/(Math.PI*2))/10+'%' : '');
  } 

  render() {

    var svg = d3.select(".chart").attr("width",700).attr("height",300);
    svg.append("g").attr("id","salesDonut");
    //svg.append("g").attr("id","quotesDonut");

    //const donut = new Donut3D()
    //donut.draw("salesDonut", this.randomData(), 150, 150, 130, 100, 30, 0.4, svg);
    //donut.draw("quotesDonut", this.randomData(), 450, 150, 130, 100, 30, 0);
   // donut.transition()
    
    // console.log(svg)

    const id = "salesDonut"
    const data = this.randomData();
    const x  = 150;
    const y = 150;
    const rx = 130;
    const ry = 100; 
    const h=30; 
    const ir=0.4;
    const radius=0.4;

     var _data = d3.pie().sort(null).value(function(d) { return d.value;})(data);

     console.log(_data)
     
    var slices = d3.select("#"+id).append("g").attr("transform", "translate(" + x + "," + y + ")")
    //var slices = d3.select("#"+id).append("g").attr("transform", "translate(" + x + "," + y + ")")
      .attr("class", "slices");

    

    slices.selectAll(".innerSlice").data(_data).enter().append("path").attr("class", "innerSlice")
      // .style("fill", function(d) { return d3.hsl(d.data.color).darker(0.7); })
      .style("fill", function(d) { console.log(d); return 'red'; })
      .attr("d",function(d){ return this.pieInner(d, rx+0.5,ry+0.5, h, ir);})
      .each(function(d){ this._current=d;});
    
    slices.selectAll(".topSlice").data(_data).enter().append("path").attr("class", "topSlice")
      .style("fill", function(d) { return d.data.color; })
      .style("stroke", function(d) { return d.data.color; })
      .attr("d",function(d){ return this.pieTop(d, rx, ry, ir);})
      .each(function(d){this._current=d;});
    
    slices.selectAll(".outerSlice").data(_data).enter().append("path").attr("class", "outerSlice")
      .style("fill", function(d) { return d3.hsl(d.data.color).darker(0.7); })
      .attr("d",function(d){ return this.pieOuter(d, rx-.5,ry-.5, h);})
      .each(function(d){console.log(d);this._current=d;});
      

    slices.selectAll(".percent").data(_data).enter().append("text").attr("class", "percent")
      .attr("x",function(d){ return 0.6*rx*Math.cos(0.5*(d.startAngle+d.endAngle));})
      .attr("y",function(d){ return 0.6*ry*Math.sin(0.5*(d.startAngle+d.endAngle));})
      .text(this.getPercent).each(function(d){this._current=d;});    

      console.log(slices)

    return (
      <div>
      Favourite Boat
      <div>
      <svg className="chart"></svg>
      </div>
         
      </div>
    );
  }
}


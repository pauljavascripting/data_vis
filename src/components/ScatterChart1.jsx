import React, {Component, PropTypes} from 'react';

import {
  ScatterChart, Scatter, XAxis, YAxis, CartesianGrid, Tooltip, ZAxis, Cell, Label
} from 'recharts';

import { scaleOrdinal } from 'd3-scale';
import { schemeCategory10 } from 'd3-scale-chromatic';

import data2 from '../data/data.json'
import data3 from '../data/data2.json'

const data = [
  { x: 100, y: 200, z:'Arapaima' },
  { x: 120, y: 100, z:'Spotted Bass' },
  { x: 170, y: 300, z:'Smallmouth Bass' },
  { x: 140, y: 250, z:'Largemouth Bass' },
  { x: 150, y: 400, z:'Carp' },
  { x: 110, y: 280, z:'Black Pacu' },
];

// point disapears onclick, so the tooltip has no rollout
const CustomTooltip = ({ active, payload, label, index }) => {
  // console.log(active)
  if (active) {
    return (
      <div className="tooltip" style={{background:'grey', padding:'0px 3px 0px 3px'}}>
        <p className="label">{`${payload[2].value}`}</p>
        <p className="label">{`${payload[0].name} : ${payload[0].value}m`}</p>
        <p className="label">{`${payload[1].name} : ${payload[1].value}kg`}</p>
      </div>
    );
  }

  return null;
};

const colors = scaleOrdinal(schemeCategory10).range();


export default class BarChart1 extends Component {

  // static propTypes = {
  //   integrationName: PropTypes.string.isRequired,
  //   connected: PropTypes.bool.isRequired,
  //   handleConnect: PropTypes.func.isRequired,
  //   handleDisconnect: PropTypes.func.isRequired,
  //   disabled: PropTypes.bool
  // };

  state = { data: data2.allFish }

  pointClicked = (entry, index) => {
    
    this.setState({data: data3.allFish})

    //document.getElementsByClassName('tooltip')[0].style.visibility = 'hidden'
    
  }

  reset = () => {

    this.setState({ data: data2.allFish })
  }

  render() {

    return (
      <div>
      Weight & Length of Fish caught
      <button onClick={this.reset}>Reset</button>
      <ScatterChart

        width={700}
        height={700}
        margin={{
          top: 20, right: 20, bottom: 20, left: 20,
        }}
      >
        <CartesianGrid />
        {/* <XAxis type="number" dataKey="x" name="length" unit="cm" />
        <YAxis type="number" dataKey="y" name="weight" unit="kg" />
        <ZAxis type="string" dataKey="z" name="name" unit="pp" />
        <Tooltip content={<CustomTooltip />} />
        <Scatter name="A school" data={data} fill="red" /> */}

        <XAxis type="number" dataKey="fish_length" name="length" unit="m">
          <Label value="Length of fish" offset={0} position="bottom" />
        </XAxis>
        <YAxis type="number" dataKey="fish_weight" name="weight" unit="kg" />
        <ZAxis type="string" dataKey="fish_species" name="name" unit="pp" />
        <Tooltip />
        <Scatter name="A school" data={this.state.data} fill="#8884d8">
          {
            this.state.data.map((entry, index) => <Cell onClick={()=>this.pointClicked(entry, index)} key={`cell-${index}`} fill={colors[index % colors.length]} />)
          }
        </Scatter>

      </ScatterChart>
      </div>
    );
  }
}


import React, {Component, PropTypes} from 'react';

import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

import data2 from '../data/data.json'

const data = [
  {
    name: '8am', uv: 4000, hours: 6, amt: 2400,
  },
  {
    name: '9am', uv: 3000, hours: 1, amt: 2210,
  },
  {
    name: '10am', uv: 2000, hours: 3, amt: 2290,
  },
  {
    name: '11am', uv: 2780, hours: 6, amt: 2000,
  },
  {
    name: '12pm', uv: 1890, hours: 5, amt: 2181,
  },
  {
    name: '1pm', uv: 2390, hours: 2, amt: 2500,
  },
  {
    name: '2pm', uv: 3490, hours: 1, amt: 2100,
  },
];

export default class LineChart1 extends Component {

  // static propTypes = {
  //   integrationName: PropTypes.string.isRequired,
  //   connected: PropTypes.bool.isRequired,
  //   handleConnect: PropTypes.func.isRequired,
  //   handleDisconnect: PropTypes.func.isRequired,
  //   disabled: PropTypes.bool
  // };

  componentDidMount(){

    console.log(data2.favouriteTime)
  }


  render() {
    return (
      <div>
      Favourite Time of Day
        <LineChart
        width={500}
        height={300}
        data={data}
        margin={{
          top: 5, right: 30, left: 20, bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Line type="monotone" dataKey="hours" stroke="#8884d8" activeDot={{ r: 8 }} />
       
      </LineChart>
      </div>
    );
  }
}


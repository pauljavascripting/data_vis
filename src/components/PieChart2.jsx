import React, {Component, PropTypes} from 'react';
import * as d3 from "d3";
const data = [1, 2, 3, 4];
const height = 400;
const width = 400;

//https://codepen.io/thecraftycoderpdx/pen/jZyzKo

export default class PieChart2 extends Component {

  // static propTypes = {
  //   integrationName: PropTypes.string.isRequired,
  //   connected: PropTypes.bool.isRequired,
  //   handleConnect: PropTypes.func.isRequired,
  //   handleDisconnect: PropTypes.func.isRequired,
  //   disabled: PropTypes.bool
  // };
  state = { data: data}

  hover = () => {

    console.log('hover')
  }

  render() {

    

    return (
      <div>
      Favourite Weather
     <div>
     <Piechart x={100} y={100} outerRadius={100} innerRadius={50}
          data={[{value: 92-34, label: 'Code lines'},
                 {value: 34, label: 'Empty lines'}]} />
     </div>
      </div>
    );
  }
}

const Piechart = ({x, y, innerRadius, outerRadius, data}) => {
    let pie = d3.pie()
                .value((d) => d.value)(data),
        translate = `translate(${x}, ${y})`,
        colors = ['red','yellow','orange']//d3.scale.category10();
    return (
        <g transform={translate}>
            {pie.map((d, i) => (
                <LabeledArc key={`arc-${i}`}
                            data={d}
                            innerRadius={innerRadius}
                            outerRadius={outerRadius}
                            color={colors(i)} />))}
        </g>
    );
};
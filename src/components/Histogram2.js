import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import * as d3 from 'd3';
import data2 from '../data/data.json'

// --------------------------------------------
//
// See the following for more detail:
// https://github.com/weknowinc/react-bubble-chart-d3
//
// --------------------------------------------

export default class Histogram2 extends Component {

  componentDidMount(){

    // const data = data2.allFish

    var margin = {top: 10, right: 30, bottom: 30, left: 40},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

    // parse the date / time
    var parseDate = d3.timeParse("%d-%m-%Y");
    // var parseDate = d3.utcParse("%d-%m-%YT%H:%M:%S.%LZ");

    // set the ranges
    var x = d3.scaleTime()
              .domain([new Date(2010, 6, 3), new Date(2012, 0, 1)])
              .rangeRound([0, width]);
    var y = d3.scaleLinear()
              .range([height, 0]);

    // set the parameters for the histogram
    var histogram = d3.histogram()
        .value(function(d) { return d.date; })
        .domain(x.domain())
        .thresholds(x.ticks(d3.timeMonth));

    // append the svg object to the body of the page
    // append a 'group' element to 'svg'
    // moves the 'group' element to the top left margin
    var svg = d3.select(".chart")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g")
        .attr("transform", 
              "translate(" + margin.left + "," + margin.top + ")");

  // format the data
  data.forEach(function(d) {
      // d.date = parseDate(d.caught_at);
      d.date = parseDate(d.dtg);
      // console.log(d)
  });

  
  // data2.allFish.forEach(function(d) {
  //     d.date = parseDate(d.caught_at.substring(0, 10));
  //     //console.log(d.date)
  // });

  // group the data for the bars ---------------------------------------------
  var bins = histogram(data);
 // var bins = histogram(data2.allFish);

  // console.log(bins)

  // Scale the range of the data in the y domain
  y.domain([0, d3.max(bins, function(d) { return d.length; })]);

  const colors = ['red','yellow','orange']
  
  // append the bar rectangles to the svg element
  svg.selectAll("rect")
      .data(bins)
    .enter().append("rect")
      .attr("class", "bar")
      .attr("x", 1)
      .attr("transform", function(d) {
      return "translate(" + x(d.x0) + "," + y(d.length) + ")"; })
      .attr("width", function(d) { return x(d.x1) - x(d.x0) -1 ; })
      .attr("height", function(d) { return height - y(d.length); })
      .attr("fill", function (d){ return d3.interpolateYlGn(d.length/1000); })
     .on("click", function(d) {
      console.log(d.x0 +' '+d.x1)
      console.log(d.length)
     })

  // add the x Axis
  svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x));

  // add the y Axis
  svg.append("g")
      .call(d3.axisLeft(y));
  
  }

  /*
 

  */
  
  render(){

    return(

      <div>
      Fish caught per month
      <div>
        <svg className="chart"></svg>      
      </div>


      </div>

    )
  }

}



 
  const data = [
   {
    "dtg": "01-01-2011",
    "info": 3
  },
  {
    "dtg": "01-01-2011",
    "info": 3
  },
  {
    "dtg": "01-01-2011",
    "info": 3
  },
  {
    "dtg": "01-01-2011",
    "info": 3.1
  },
  {
    "dtg": "01-01-2011",
    "info": 3.1
  },
  {
    "dtg": "01-01-2011",
    "info": 3.2
  },
  {
    "dtg": "01-01-2011",
    "info": 3.2
  },
  {
    "dtg": "01-01-2011",
    "info": 3.2
  },
  {
    "dtg": "01-01-2011",
    "info": 3.3
  },
  {
    "dtg": "01-01-2011",
    "info": 3.3
  },
]


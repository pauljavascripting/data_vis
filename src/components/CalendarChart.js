import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import CalendarHeatmap from 'react-calendar-heatmap';
import '../App.scss';
import data2 from '../data/data.json'
const _ = require('lodash');

// --------------------------------------------
//
// See the following for more detail:
// https://github.com/weknowinc/react-bubble-chart-d3
//
// --------------------------------------------

export default class CalendarChart extends Component {
  
  state = { day:'', count:null, type:''}

  componentDidMount() {

    // console.log(data2.calendarFish.children)
   
  }

  componentDidUpdate() {
   
  }

  onClick = (date) => {

    // var rooms = [
    //   { title: 'Just For Fun', created: '2016-10-23T16:57:03.288Z', id: 2 },
    //   { title: 'Development', created: '2016-10-23T16:57:03.294Z', id: 6 }
    // ];
    // var room = _.find(rooms, {'id': 6});
    // console.log(room); // Object {title: "Development", created: "2016-10-23T16:57:03.294Z", id: 6}


    // var rooms = [

    //      { date: '2018-01-01T00:00:00.000Z', count: 1},
    //      { date: '2018-01-02T00:00:00.000Z', count: 3},
    //      { date: '2018-01-03T00:00:00.000Z', count: 4, fish_type: 'coley'  }

    // ]

    // var room = _.find(rooms, {date:'2018-01-03T00:00:00.000Z'})
    // console.log(room) // search array for fish from thts day using lodash

    // const test = _.find(data2.calendarFish, { 'date': date.date })
    //console.log(test)

    // const info = _.find(data2.calendarFish.children, { 'created_at': date.date })
    const info = _.filter(data2.calendarFish.children, function(p) {
      return p.created_at === date.date;
    });

    console.log(info)

    if(date){

      this.setState({day:date.date, count:date.count, type:date.fish_type})

    }
    else{

     this.setState({day:'no data', count:0})
    }
    
  }

  render(){

    return(

      <div>
      Fish caught per day
      <div>
      <CalendarHeatmap
        onClick={this.onClick}
        startDate={new Date('2019-01-31')}
        endDate={new Date('2019-12-31')}
        values={ data2.calendarFish.parent }
        // startDate={new Date('2017-12-31')}
        // endDate={new Date('2018-12-31')}
        // values={[
        //  { date: '2018-01-01T00:00:00.000Z', count: 1, fish_type: 'carp' },
        //  { date: '2018-01-02T00:00:00.000Z', count: 3, fish_type: 'dogfish'  },
        //  { date: '2018-01-03T00:00:00.000Z', count: 4, fish_type: 'coley'  },
        //  { date: '2018-01-05', count: 2, fish_type: 'carp'  },
        //  { date: '2018-01-06', count: 5, fish_type: 'carp'  },
        //  { date: '2018-01-07', count: 5, fish_type: 'carp'  },
        //  { date: '2018-01-09', count: 2, fish_type: 'carp'  },
        //  { date: '2018-01-10', count: 1, fish_type: 'carp'  },
        //  { date: '2018-01-11', count: 3, fish_type: 'carp'  },
        //  { date: '2018-01-12', count: 2, fish_type: 'carp'  },
        //  { date: '2018-01-13', count: 1, fish_type: 'carp'  },
        //  { date: '2018-01-14', count: 5, fish_type: 'carp'  },
        //  { date: '2018-01-15', count: 2, fish_type: 'carp'  },
        //  { date: '2018-01-16', count: 3, fish_type: 'carp'  },
        //  { date: '2018-01-17', count: 3, fish_type: 'carp'  },
        //  { date: '2018-01-18', count: 5, fish_type: 'carp'  },
        //  { date: '2018-01-19', count: 4, fish_type: 'carp'  },
        //  { date: '2018-01-20', count: 2, fish_type: 'carp'  },
        //  { date: '2018-01-21', count: 3, fish_type: 'carp'  },
        //  { date: '2018-01-22', count: 4, fish_type: 'carp'  },
        //  { date: '2018-01-23', count: 5, fish_type: 'carp'  },
        //  { date: '2018-01-24', count: 4, fish_type: 'carp'  },
        //  { date: '2018-01-25', count: 1, fish_type: 'carp'  },
        //  { date: '2018-01-26', count: 3, fish_type: 'carp'  },
        //  { date: '2018-01-27', count: 2, fish_type: 'carp'  },
        //  { date: '2018-01-28', count: 3, fish_type: 'carp'  },
        //  { date: '2018-01-29', count: 2, fish_type: 'carp'  },
        //  { date: '2018-01-30', count: 4, fish_type: 'carp'  },

        //  { date: '2018-02-06', count: 2, fish_type: 'carp'  },
        //  { date: '2018-04-10', count: 10, fish_type: 'carp'  },
        //  { date: '2018-05-11', count: 3, fish_type: 'carp'  },
        //  { date: '2018-06-12', count: 5, fish_type: 'carp'  },
        //  { date: '2018-08-13', count: 0, fish_type: 'carp'  },

          
        // ]}
        classForValue={(value) => {
          if (!value) {
            return 'color-empty';
          }
          return `color-gitlab-${value.count}`;
        }}
      />

      </div>

      <div>
      {this.state.day} : {this.state.count}
      </div>

      </div>

    )
  }
}
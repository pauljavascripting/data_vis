import React, {Component, PropTypes} from 'react';

import {
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

import data2 from '../data/data.json'

const data = [
  {
    weather: "Clear Sky", time_spent_in_event: 28914.048954, time_spent_fishing: 1433.860783, total_fish: 50, total_length: 26.028986, total_weight: 358.213174
  },
  {
    weather: "Thunder", time_spent_in_event: 28914.048954, time_spent_fishing: 233.860783, total_fish: 50, total_length: 26.028986, total_weight: 358.213174
  },
  {
    weather: "Rain", time_spent_in_event: 28914.048954, time_spent_fishing: 433.860783, total_fish: 50, total_length: 26.028986, total_weight: 358.213174
  },
  {
    weather: "Cloud", time_spent_in_event: 28914.048954, time_spent_fishing: 33.860783, total_fish: 50, total_length: 26.028986, total_weight: 358.213174
  },
  {
    weather: "Fog", time_spent_in_event: 28914.048954, time_spent_fishing: 1643.860783, total_fish: 50, total_length: 26.028986, total_weight: 358.213174
  },
  {
    weather: "Sunny", time_spent_in_event: 28914.048954, time_spent_fishing: 133.860783, total_fish: 50, total_length: 26.028986, total_weight: 358.213174,
  },
  {
    weather: "Snowing", time_spent_in_event: 28914.048954, time_spent_fishing: 1433.860783, total_fish: 50, total_length: 26.028986, total_weight: 358.213174,
  },
];

export default class BarChart2 extends Component {

  // static propTypes = {
  //   integrationName: PropTypes.string.isRequired,
  //   connected: PropTypes.bool.isRequired,
  //   handleConnect: PropTypes.func.isRequired,
  //   handleDisconnect: PropTypes.func.isRequired,
  //   disabled: PropTypes.bool
  // };

  componentDidMount(){

    console.log(data2.favouriteWeather)
  }

  render() {
    return (
      <div>
      Favourite Weather
       <BarChart
        width={800}
        height={500}
        data={data}
        margin={{
          top: 5, right: 30, left: 20, bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="weather" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey="time_spent_fishing" fill="#8884d8" />
       
      </BarChart>
      </div>
    );
  }
}


import React, {Component, PropTypes} from 'react';

import {
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

import data2 from '../data/data.json'

import { scaleOrdinal } from 'd3-scale';
import { schemeCategory10 } from 'd3-scale-chromatic';


const colors = scaleOrdinal(schemeCategory10).range();



const TriangleBar = (props) => {
  const {
    fill, x, y, width, height,
  } = props;

  return <path d={getPath(x, y, width, height)} stroke="none" fill={fill} />;
};

// TriangleBar.propTypes = {
//   fill: PropTypes.string,
//   x: PropTypes.number,
//   y: PropTypes.number,
//   width: PropTypes.number,
//   height: PropTypes.number,
// };

const getPath = (x, y, width, height) => `M${x},${y + height}
          C${x + width / 3},${y + height} ${x + width / 2},${y + height / 3} ${x + width / 2}, ${y}
          C${x + width / 2},${y + height / 3} ${x + 2 * width / 3},${y + height} ${x + width}, ${y + height}
          Z`;

export default class BarChart2 extends Component {

  // static propTypes = {
  //   integrationName: PropTypes.string.isRequired,
  //   connected: PropTypes.bool.isRequired,
  //   handleConnect: PropTypes.func.isRequired,
  //   handleDisconnect: PropTypes.func.isRequired,
  //   disabled: PropTypes.bool
  // };

  render() {
    return (
      <div>
      Fish Discovered
       <BarChart
        width={800}
        height={600}
        data={data2.fishDiscovered}
        margin={{
          top: 5, right: 30, left: 20, bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="fish_species" angle={-90} textAnchor="end" height={150} interval={0} />
        <YAxis />
        <Tooltip />
        {/* <Legend /> */}
        {/* <Bar dataKey="count" fill="#8884d8" /> */}

        {/* <Bar dataKey="count" fill="#8884d8" shape={<TriangleBar />} label={{ position: 'top' }}> */}
        <Bar dataKey="count" fill="#8884d8" label={{ position: 'top' }}>
          {
            data2.fishDiscovered.map((entry, index) => (
              <Cell key={`cell-${index}`} fill={colors[index % 10]} />
            ))
          }
        </Bar>
       
      </BarChart>
      </div>
    );
  }
}


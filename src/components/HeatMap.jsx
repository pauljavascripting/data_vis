import React, { Component } from 'react';
import { Map, CircleMarker, ImageOverlay } from "react-leaflet";
import {CRS} from 'leaflet';
import axios from 'axios'
import HeatmapLayer from 'react-leaflet-heatmap-layer'
import "leaflet/dist/leaflet.css";
import '../App.css'

//-------------------------------------------------------------------
//
// 1. Component uses React-Leaflet's Map component
// 2. Adds an image overlay 
// 3. Places an associated heatmap layer over the image using a json file
//
//-------------------------------------------------------------------

export default class HeatMap extends Component {

  state = { 
    
    array:[], 
    mapImg:this.props.image, 
    blur:this.props.blur,
    bounds:this.props.bounds, 
    centre:this.props.centre, 
    dragging:this.props.dragging, 
    gradient:this.props.gradient,
    json:this.props.json, 
    minZoom:this.props.minZoom, 
    maxZoom:this.props.maxZoom, 
    posX:this.props.posX, 
    posY:this.props.posY, 
    posXDivider:this.props.posXDivider, 
    posYDivider:this.props.posYDivider,
    radius:this.props.radius,
    zoom:this.props.zoom, 
  }

  // static propTypes = {
    
  //   dragging: PropTypes.bool
  // };


  componentDidMount(){
    
    axios.get(this.state.json)
      
      .then((response) => {

        const data = response.data

        const array = []

        for(let i=0;i<data.length;i++){

          const tempArray = []
          tempArray.push((data[i].x+this.state.posX)/this.state.posXDivider)
          tempArray.push((data[i].y+this.state.posY)/this.state.posYDivider)

          array.push(tempArray)

        }
        //console.log(array.length)
        this.setState({array:array})

      })

  }

  render() {

    return (
        <div style={{ height: '100vh', width: '100%' }}>
        <Map
          style={{ height: "100%", width: "100%" }}
          zoom={this.state.zoom}
          center={this.state.centre}
          maxBoundsViscosity={1}
          ref={(ref) => { this.map = ref; }}
          onMove={this.onUpdate}
          minZoom={this.state.minZoom}
          maxZoom={this.state.maxZoom}
          dragging={this.state.dragging}
          crs={CRS.Simple}
          >
          
           <ImageOverlay
                bounds={this.state.bounds}
                url={this.state.mapImg}
                transparent={true}
                opacity={0.99}
                className='overlay'
            />

             <HeatmapLayer
                   fitBoundsOnLoad
                    points={this.state.array}
                    longitudeExtractor={m => m[1]}
                    latitudeExtractor={m => m[0]}
                    intensityExtractor={m => parseFloat(m[2])}
                    radius={this.state.radius}
                    gradient={this.state.gradient}
                    blur={this.state.blur}
               />

          {/*<CircleMarker
            center={this.state.centre}
          />*/}
        
        </Map>
      </div>
    );
  }
}


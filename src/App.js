import React from 'react';
// import Chart from './components/BarChart2' // favourite weather - working!!!!
// import Chart from './components/BarChart3' // fish discovered - working !!!!
// import Chart from './components/BubbleChart2' // fish caught per day - working !!!
// import Chart from './components/LineChart2' // favourite time - working!!!!
// import Chart from './components/LiquidGauge1'
// import Chart from './components/ScatterChart1' // fish height & weight - working!!!!
// import Chart from './components/PercentageChart1' // boss fish discovered
// import Chart from './components/PictographChart1' // num of tournaments won - working !!!
import Chart from './components/PieChart3' // fish species discovered - group species
// import Chart from './components/HeatMap1'
// import Chart from './components/HeatMap13'
// import Chart from './components/CalendarChart' // fish caught per day - working!!!!
// import Chart from './components/Histogram2' // fish caught per month - can only handle day & num of fish caught
// import Chart from './components/CirclePacking'

// import Chart from './components/DonutChart1'

// results > progressarc & tile

// look at the data & see what graphs are applicable!!


function App() {
  return (
    <div className="App">
     <Chart />
    </div>
  );
}

export default App;
